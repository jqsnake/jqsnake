function Canvas() {
    this.coefPropW = 1;
    this.coefPropH = 1;
	
	/*--~ Functions ~--*/
	this.init = function() {
		$('#gameView').delay(350).fadeToggle();
		
		$("#gameCanvas").drawImage({
			source: "img/boot.png",
			x: 0,
			y: 0,
			fromCenter: false
		});
	}
	this.refresh = function() {
		this.drawSquare(game.players[0].position[0],game.players[0].position[1],'#0000FF');
        this.drawSquare(game.players[1].position[0],game.players[1].position[1],'#FF69B4');
	};
	this.drawSquare = function(x_a,y_a,color) {
		$("#gameCanvas").drawRect({
			fillStyle: color,
			x: x_a*10 *this.coefPropW, y: y_a*10*this.coefPropH,
			width: 10 *this.coefPropW,
			height: 10 *this.coefPropH,
			fromCenter: false
		});
	};
	this.clearCanvas = function() {
		$('#gameCanvas').clearCanvas();
	};
	this.getPNG = function() {
		return $("#gameCanvas").getCanvasImage('png');
	};
	this.resize = function(width,height) {
        if(width <= 500) {
            $('#gameCanvas').attr('width',width);
            $('#gameCanvas').css('margin-left',parseInt((940 - width) / 2));
        }
        else {
            this.coefPropW = 500/width;
            $('#gameCanvas').attr('width',500);
            $('#gameCanvas').css('margin-left',220);
        }
        
        if(height <= 500) {
            $('#gameCanvas').attr('height',height);
        }
        else {
            this.coefPropH = 500/height;
            $('#gameCanvas').attr('height',500);
        }
    };
}