function Bots() {    
	/*--~ Functions ~--*/
    
	this.doStrategy = function (player) {
		switch (player.strategy) {
			case 1:
				return this.strategy1(player);
				break;
			case 2:
				return this.strategy2(player);
				break;
			case 3:
				return this.strategy3(player);
				break;
            case 4:
				return this.strategy4(player);
				break;
            case 5:
				return this.strategy5(player);
				break;
            case 6:
				return this.strategy6(player);
				break;
            case 7:
				return this.strategy7(player);
				break;
            case 8:
				return this.strategy8(player);
				break;
            default:
                return player.direction;
                break;
		}
	};
	
	this.turnLeft = function(player){
		switch(player.direction) {
			case 'UP':
				return 'LEFT';
				break;
			case 'LEFT':
				return 'DOWN';
				break;
			case 'DOWN':
				return 'RIGHT';
				break;
			case 'RIGHT':
				return 'UP';
				break;
		}
		return;
	};
	
	this.turnRight = function(player){
		switch(player.direction) {
			case 'UP':
				return 'RIGHT';
				break;
			case 'LEFT':
				return 'UP';
				break;
			case 'DOWN':
				return 'LEFT';
				break;
			case 'RIGHT':
				return 'DOWN';
				break;
		}
		return;
	};
	
    
    
    this.strategy1 = function(player) {
        var newDirection = player.direction;
		switch(Math.floor(Math.random()*2.9)) {
			case 0:
				newDirection = this.turnLeft(player);
				break;
			case 1:
				newDirection = this.turnRight(player);
				break;
			default:
				newDirection = player.direction;
				break;
			}
        return newDirection;
	};
    
	this.strategy2 = function(player){
        var tableDir = ['UP', 'DOWN', 'LEFT', 'RIGHT']
        for (var i = 0; i < 4; i++) {
            if (map.isClearDirection(player, tableDir[i])){
                return tableDir[i];
                break;
            }
        }
	};
	
    this.strategy3 = function(player) {
        var newDirection = player.direction;
        switch (Math.floor(Math.random()*4.5)) { 
            case 1:
                newDirection = this.turnLeft(player);
                break;
            case 2:
                newDirection = this.turnRight(player);
                break;
            default:
                newDirection = player.direction;
                break;
        }
        return newDirection;
    };
    
    this.strategy4 = function(player) {
        var newDirection = player.direction;
        var random = Math.floor(Math.random()*10);
        if (map.isClearDirection(player, this.turnLeft(player)) && random == 0){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player)) && random == 1){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, player.direction)){
            newDirection = player.direction;
        }
        else if (map.isClearDirection(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        return newDirection; 
    };
    
    this.strategy5 = function(player) {
        var newDirection = player.direction;
        var random = Math.floor(Math.random()*1.999);
        
        if (map.isClearDirection(player, player.direction)){
            newDirection = player.direction;
        }
        else if (map.isClearDirection(player, this.turnLeft(player)) && random == 0){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player)) && random == 1){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        return newDirection;
    };
    
    this.strategy6 = function(player) {
        var newDirection = player.direction;
        var random = Math.floor(Math.random()*1.999);
        
        if (random == 0 && map.getDirectionToOtherx(player) != false && map.isClearDirection(player, map.getDirectionToOtherx(player))) {
                newDirection = map.getDirectionToOtherx(player);
        }
        else if (random == 1 && map.getDirectionToOthery(player) != false && map.isClearDirection(player, map.getDirectionToOthery(player))) {
                newDirection = map.getDirectionToOthery(player);
        }
        else if (map.getDirectionToOtherx(player) != false && map.isClearDirection(player, map.getDirectionToOtherx(player))) {
                newDirection = map.getDirectionToOtherx(player);
        }
        else if (map.getDirectionToOthery(player) != false && map.isClearDirection(player, map.getDirectionToOthery(player))) {
                newDirection = map.getDirectionToOthery(player);
        }   
        else if (map.isClearDirection(player, player.direction)){
            newDirection = player.direction;
        }
        else if (map.isClearDirection(player, this.turnLeft(player)) && random == 0){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player)) && random == 1){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        return newDirection;
    }; 
    
    this.strategy7 = function(player) { 
        var newDirection = player.direction;
        var random = Math.floor(Math.random()*1.999);
        // a refaire : trop chaotique
        if (random == 0 && map.isClearDirection(player, map.getDirectionFromOtherx(player)) && map.isNotOneWay(player, map.getDirectionFromOtherx(player))) {
                newDirection = map.getDirectionFromOtherx(player);
        }
        else if (random == 1 && map.isClearDirection(player, map.getDirectionFromOthery(player))  && map.isNotOneWay(player, map.getDirectionFromOthery(player))) {
                newDirection = map.getDirectionFromOthery(player);
        }
        else if ( map.isClearDirection(player, map.getDirectionFromOtherx(player)) && map.isNotOneWay(player, map.getDirectionFromOtherx(player))) {
                newDirection = map.getDirectionFromOtherx(player);
        }
        else if (map.isClearDirection(player, map.getDirectionFromOthery(player)) && map.isNotOneWay(player, map.getDirectionFromOthery(player))) {
                newDirection = map.getDirectionFromOthery(player);
        }   
        else if (map.isClearDirection(player, player.direction) && map.isNotOneWay(player, player.direction)){
            newDirection = player.direction;
        }
        else if (map.isClearDirection(player, this.turnLeft(player)) && random == 0 && map.isNotOneWay(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player)) && random == 1 && map.isNotOneWay(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player))&& map.isNotOneWay(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player))&& map.isNotOneWay(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player)) && random == 0){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player)) && random == 1){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        return newDirection;

    };
    
    this.strategy8 = function(player) { 
        var newDirection = player.direction;
        var random = Math.floor(Math.random()*1.999);
        
        if (map.isClearDirection(player, player.direction) && map.isNotOneWay(player, player.direction)){
            newDirection = player.direction;
        }
        else if (map.isClearDirection(player, this.turnLeft(player)) && random == 0 && map.isNotOneWay(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player)) && random == 1 && map.isNotOneWay(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player))&& map.isNotOneWay(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player))&& map.isNotOneWay(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, player.direction)){
            newDirection = player.direction;
        }
        else if (map.isClearDirection(player, this.turnLeft(player)) && random == 0){
            newDirection = this.turnLeft(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player)) && random == 1){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnRight(player))){
            newDirection = this.turnRight(player);
        }
        else if (map.isClearDirection(player, this.turnLeft(player))){
            newDirection = this.turnLeft(player);
        }
        return newDirection;

    };
        
}