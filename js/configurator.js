function Configurator() {
	/*--~ Functions ~--*/
	this.init = function() {
		if ($.jCookies({ get : 'p1' })) {
			if ($.jCookies({ get : 'p1' }) == 'bot') {
				$('#bp1').button('toggle');
			} else {
				$('#hp1').button('toggle');
			}
		} else {
			$('#hp1').button('toggle');
		}

		if ($.jCookies({ get : 'p2' })) {
			if ($.jCookies({ get : 'p2' }) == 'bot') {
				$('#bp2').button('toggle');
			} else {
				$('#hp2').button('toggle');
			}
		} else {
			$('#hp2').button('toggle');
		}

		if ($.jCookies({ get : 'strat1' })) {
			// $("#monSelect option[value='val1']").attr('selected','selected');
			$('#strategy2').val($.jCookies({ get : 'strat1' }));
		}

		// Set the acitons for the play button
		$('#playButton').click(function () {
			$('#configPanel').fadeToggle();
            keyboard.setLayout($('#keyboard').val());
			configurator.launchGame();
		});
	};
    
    
	this.launchGame = function() {
        // Strategie
        var ia1 = false;
		var ia2 = false;
        
		if($('#bp1').hasClass('active')) {
			ia1 = parseInt($('#strategy1').val());
			$.jCookies({name:'p1', value:'bot'});
		} else {
			$.jCookies({name:'p1', value:'human'});
		}

		if($('#bp2').hasClass('active')) {
			ia2 = parseInt($('#strategy2').val());
			$.jCookies({name:'p2', value:'bot'});
		} else {
			$.jCookies({name:'p2', value:'human'});
		}

		$.jCookies({name:'strat1',value:$('#strategy2').val()});
		
        // Add the players
            game.addPlayer({
				identifier:1, 
				bot: $('#bp1').hasClass('active'), 
				ia: ia1, 
				position: [2,Math.floor(map.y/2)], 
				direction: 'RIGHT'
			});
            game.addPlayer({
				identifier:2, 
				bot: $('#bp2').hasClass('active'), 
				ia: ia2, 
				position: [map.x-3,Math.floor(map.y/2)], 
				direction: 'LEFT'
			});
        
		//temps
		switch ($('#speed option:selected').val()) {
			case 'slow':
				timer.period = 300;
				break;
			case 'normal':
				timer.period = 40;
				break;
			case 'fast':
				timer.period = 1;
				break;
			default:
				timer.period = 40;
				break;
		}

		canvas.init();
	};
	this.colorButton = function(button,cssClass) {
		$(button).click(function() {
			if ($(button).hasClass('active')) {
				$(button).removeClass(cssClass);
			}
			else {
				$(button).addClass(cssClass);
			}
		});
	};
}