function Map() {
	/*--~ Variables ~--*/
    this.x = 74;
    this.y = 74;
	this.map = Array(this.x);

	/*--~ Functions ~--*/
	this.makeMap = function() {
		for (var x = this.x;x--;) {
			this.map[x] = Array(this.y);
			for (var y = this.y; y--;) {
				this.map[x][y] = 0;
			};
		};
	}
	this.makeDots = function() {
        this.map[game.players[0].position[0]][game.players[0].position[1]] = 1;
		this.map[game.players[1].position[0]][game.players[1].position[1]] = 2;
	}
    
    //envirronements des bots
    
	this.isClear = function(x,y) {
		if (x>map.x-1 || x<0 || y>map.y-1 || y<0) {
			return false;
		}        
        else if (this.map[x][y] != 0){
            return false;
        }
        else {
			return true;
		}
	}
    
	this.isNotOneWay = function(player, direction) {
		var nextPlayer;
        nextPlayer = {position: Array(2), direction: 'NONE'};
		switch (direction) {
            case 'UP':
                nextPlayer.position[0] = (player.position[0]);
                nextPlayer.position[1] = (player.position[1]) - 1;
				nextPlayer.direction = 'UP';
                break;
            case 'DOWN':
                nextPlayer.position[0] = (player.position[0]);
                nextPlayer.position[1] = (player.position[1]) + 1;
				nextPlayer.direction = 'DOWN';
                break;
            case 'LEFT':
                nextPlayer.position[0] = (player.position[0]) - 1;
                nextPlayer.position[1] = (player.position[1]);
				nextPlayer.direction = 'LEFT';
                break;
            case 'RIGHT':
                nextPlayer.position[0] = (player.position[0]) + 1;
                nextPlayer.position[1] = (player.position[1]);
				nextPlayer.direction = 'RIGHT';
                break;
        }
		if (this.isClearDirection(nextPlayer, bots.turnLeft(nextPlayer)) || this.isClearDirection(nextPlayer, bots.turnLeft(nextPlayer))) {
			return true;
		}
		else {
			return false;
		}   
	};
	
    this.isClearDirection = function (player, direction) {
        switch (direction) {
            case 'UP':
                return this.isClear(player.position[0],player.position[1]-1);
                break;
            case 'DOWN':
                return this.isClear(player.position[0],player.position[1]+1);
                break;
            case 'LEFT':
                return this.isClear(player.position[0]-1,player.position[1]);
                break;
            case 'RIGHT':
                return this.isClear(player.position[0]+1,player.position[1]);
                break;
        }
    };
    
    this.getDirectionToOtherx = function(player) {
        var position = this.getOtherPlayerPosition(player, 'x');
        if (position - player.position[0] < 0) {
            return 'LEFT';
        }
        if (position - player.position[0] > 0) {
            return 'RIGHT';
        }
        else {
            return false;
        }
    
    };
    
    this.getDirectionToOthery = function(player) {
        var position = this.getOtherPlayerPosition(player, 'y');
        if (position - player.position[1] < 0) {
            return 'UP';
        }
        if (position - player.position[1] > 0) {
            return 'DOWN';
        }
        else {
            return false;
        }
    
    };
    
    this.getDirectionFromOtherx = function(player) {
        var position = this.getOtherPlayerPosition(player, 'x');
        var random = Math.floor(Math.random()*1,999);
        if (position - player.position[0] > 0) {
            return 'LEFT';
        }
        else if (position - player.position[0] < 0) {
            return 'RIGHT';
        }
        else if (position - player.position[0] == 0){
            switch (random) {
                case 0:
                    return 'LEFT';
                    break;
                case 1:
                    return 'RIGHT';
                    break;
            }
        }
    
    };
    
    this.getDirectionFromOthery = function(player) {
        var position = this.getOtherPlayerPosition(player, 'y');
        var random = Math.floor(Math.random()*1,999);
        if (position - player.position[1] > 0) {
            return 'UP';
        }
        else if (position - player.position[1] < 0) {
            return 'DOWN';
        }
        else if (position - player.position[1] == 0){
            switch (random) {
                case 0:
                    return 'UP';
                    break;
                case 1:
                    return 'DOWN';
                    break;
            }
        }
    
    };
    
    this.getOtherPlayerPosition = function(player, axe) {
        if (player.identifier == 1) {
            switch (axe) {
                case 'x':
                    return game.players[1].position[0];
                    break;
                case 'y':
                    return game.players[1].position[1];
                    break;
            }
        }
        else if (player.identifier == 2) {
            switch (axe) {
                case 'x':
                    return game.players[0].position[0];
                    break;
                case 'y':
                    return game.players[0].position[1];
                    break;
            }
        }
    };
    
    
}