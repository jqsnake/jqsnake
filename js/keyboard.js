
function Keyboard(layout) {
	/*--~ Variables ~--*/
	this.layout;
    
    this.setLayout = function(layout) {
		if (layout == 'AZERTY') {
			this.setAZERTY();
		}
		else if (layout == 'QWERTY') {
			this.setQWERTY();
		}
        this.layout = layout;
	};

	/*--~ Functions ~--*/
    
	this.setAZERTY = function() {
		$(document).keypress(function(event){
			if (event.keyCode == 13 && game.launched == false) {
				game.startGame();
			}
			else if (game.launched == true) {
				switch (event.charCode) {
					case 32:
						if (!game.ended && !game.paused) {
							game.toggleGame();
						}
						break;
                        
                    case 110:
						if (!game.ended && !game.paused) {
							game.stopGame();
						}
						break;

					// HP1 AZERTY KEYS
                    case 122:
                        if (!game.players[0].bot) {
                            game.sendKey('UP',1);
                        }
                        break;
                    case 115:
                        if (!game.players[0].bot) {
                            game.sendKey('DOWN',1);
                        }
                        break;
                    case 113:
                        if (!game.players[0].bot) {
                            game.sendKey('LEFT',1);
                        }
                        break;
                    case 100:
                        if (!game.players[0].bot) {
                            game.sendKey('RIGHT',1);
                        }
                        break;

					// HP2 AZERTY KEYS
                    case 105:
                        if (!game.players[1].bot) {
                            game.sendKey('UP',2);
                        }
                        break;
                    case 107:
                        if (!game.players[1].bot) {
                            game.sendKey('DOWN',2);
                        }
                        break;
                    case 106:
                        if (!game.players[1].bot) {
                            game.sendKey('LEFT',2);
                        }
                        break;
                    case 108:
                        if (!game.players[1].bot) {
                            game.sendKey('RIGHT',2);
                        }
                        break;
					// AZERTY DEFAULT
					default:
						console.log('Touche non trouvée : ' + event.charCode);
						break;
				}
			}
		});
	};
    
    //QWERTY 
	this.setQWERTY = function() {
		$(document).keypress(function(event){
			if (event.keyCode == 13 && game.launched == false) {
				game.startGame();
			}
			else if (game.launched == true) {
				switch (event.charCode) {
					case 32:
						game.toggleGame();
						break;
                        
                    case 76:
						if (!game.ended && !game.paused) {
							game.stopGame();
						}
						break;

					// HP1 QWERTY KEYS
                    case 119:
                        if (!game.players[0].bot) {
                            game.sendKey('UP',1);
                        }
                        break;
                    case 115:
                        if (!game.players[0].bot) {
                            game.sendKey('DOWN',1);
                        }
                        break;
                    case 97:
                        if (!game.players[0].bot) {
                            game.sendKey('LEFT',1);
                        }
                        break;
                    case 100:
                        if (!game.players[0].bot) {
                            game.sendKey('RIGHT',1);
                        }
                        break;
                    
					// HP2 QWERTY KEYS
                    case 116:
                        if (!game.players[1].bot) {
                            game.sendKey('UP',2);
                        }
                        break;
                    case 103:
                        if (!game.players[1].bot) {
                            game.sendKey('DOWN',2);
                        }
                        break;
                    case 102:
                        if (!game.players[1].bot) {
                            game.sendKey('LEFT',2);
                        }
                        break;
                    case 104:
                        if (!game.players[1].bot) {
                            game.sendKey('RIGHT',2);
                        }
                        break;

					// QWERY DEFAULT
					default:
						console.log('Touche non trouvée : ' + event.charCode);
						break;
				}
			}
		});
	};
}