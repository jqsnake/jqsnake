function Game() {
	/*--~ Variables ~--*/
	this.launched = false;
	this.paused = false;
	this.players = [];
	this.fast = false;
	this.ended = false;
    this.stoped = false;

	/*--~ Functions ~--*/
    this.startGame = function() {
		notification.blueNote('Jeu lancé');
		this.launched = true;
        canvas.clearCanvas();
        canvas.resize((map.x)*10, (map.y)*10);
		map.makeMap();
        map.makeDots();
        canvas.refresh();
		timer.startTimer();

		$('#pauseModal').on('hidden', function() {
			game.toggleGame();
		});
	};
	this.toggleGame = function() {
		if (!this.paused) {
			this.paused = true;
			$('#pauseModal').modal('show');
			timer.stopTimer();
		}
		else {
			this.paused = false;
			$('#pauseModal').modal('hide');
			timer.startTimer();
		}
	};
    this.stopGame = function() {
		if (!this.stoped) {
            timer.stopTimer();
		} else {
			timer.startTimer();
		}
		this.stoped = !this.stoped;
	};
    
    this.opposite = function(direction) {
        switch (direction) {
            case 'UP':
                return 'DOWN';
                break;
            case 'DOWN':
                return 'UP';
                break;
            case 'LEFT':
                return 'RIGHT';
                break;
            case 'RIGHT':
                return 'LEFT';
                break;
        }
    };
    
    this.changeDirectionPossible = function(direction, player) {
        if (direction != this.opposite(this.players[player].direction)) {
            return true;
        }
    
    };
    
	this.sendKey = function(direction,player) {
        if (this.changeDirectionPossible(direction, player-1)) {
            this.players[player-1].direction = direction;
        }
	};
    
	this.addPlayer = function(player) {
		this.players.push(new Player(player));
	};
    
	this.gameCycle = function() {
    
        //ia
        if (this.players[0].bot) {
            this.sendKey(bots.doStrategy({
                strategy : this.players[0].ia,
                identifier : this.players[0].identifier,
                position : this.players[0].position,
                direction : this.players[0].direction
            }),this.players[0].identifier);
        }
        if (this.players[1].bot) {
            this.sendKey(bots.doStrategy({
                strategy : this.players[1].ia,
                identifier : this.players[1].identifier,
                position : this.players[1].position,
                direction : this.players[1].direction
            }),this.players[1].identifier);
        }
        
        //crash
        this.players[0].MeditatePosition();
        this.players[1].MeditatePosition();
            
		if (!this.checkLife()) {
			timer.stopTimer();
			conclusion.conclusion();
			return;
		}
		map.makeDots();
		canvas.refresh();
	};
	this.checkLife = function() {
		return this.players[0].alive && this.players[1].alive;
	};
}